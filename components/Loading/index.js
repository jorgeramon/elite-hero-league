import PropTypes from "prop-types";
import styles from "./styles.module.css";

const Loading = ({ show }) =>
  show ? (
    <div className={styles.container}>
      <div className={styles.spinner}>
        <div></div>
        <div className={styles.rect2}></div>
        <div className={styles.rect3}></div>
        <div className={styles.rect4}></div>
        <div className={styles.rect5}></div>
      </div>
    </div>
  ) : null;

Loading.propTypes = {
  show: PropTypes.bool.isRequired,
};

export default Loading;
