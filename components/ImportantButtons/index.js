import classNames from "classnames";
import RegisteredContext from "context/registered";
import { useContext } from "react";
import styles from "./styles.module.css";

export default function ImportantButtons() {
  const { isRegistered } = useContext(RegisteredContext);

  return (
    <>
      <div className="d-flex flex-column flex-wrap w-100 justify-content-center align-items-center">
        {/*isRegistered ? null : (
          <button
            className={classNames(
              "btn btn-lg text-white important-btn",
              styles["scores-btn"]
            )}
            type="button"
            data-bs-toggle="modal"
            data-bs-target="#staticBackdrop"
          >
            <i className="fas fa-trophy me-2"></i> Enviar puntuaciones
          </button>
            )*/}

        <a
          href="https://www.facebook.com/Arvelplay0907-335239957873868/"
          role="button"
          className={classNames(
            "btn btn-lg text-white mt-4 important-btn",
            styles["scores-btn"]
          )}
        >
          <i className="fab fa-facebook-square me-2"></i> Página de
          Arvelplay0907
        </a>

        <a
          href="https://discord.gg/nnFbCqEG2D"
          role="button"
          className={classNames(
            "btn btn-lg text-white mt-4 important-btn",
            styles["discord-btn"]
          )}
        >
          <i className="fab fa-discord me-2"></i> Plastic Hero Community
        </a>

        <a
          href="https://discord.gg/FdWB3XvWEj"
          role="button"
          className={classNames(
            "btn btn-lg text-white mt-4 mb-5 important-btn",
            styles["discord-btn"]
          )}
        >
          <i className="fab fa-discord me-2"></i> Elite Hero League
        </a>
      </div>
    </>
  );
}
