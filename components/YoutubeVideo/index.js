import PropTypes from "prop-types";
import styles from "./styles.module.css";

const YoutubeVideo = ({ embedId }) => (
  <div className={styles["video-responsive"]}>
    <iframe
      className={styles["video-responsive-iframe"]}
      src={`https://www.youtube.com/embed/${embedId}`}
      frameBorder="0"
      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
      allowFullScreen
      title="Embedded youtube"
    />
  </div>
);

YoutubeVideo.propTypes = {
  embedId: PropTypes.string.isRequired,
};

export default YoutubeVideo;
