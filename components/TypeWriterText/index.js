import classNames from "classnames";
import { useEffect } from "react";
import TypeWriter from "typewriter-effect/dist/core";
import styles from "./styles.module.css";

export default function TypeWriterText() {
  useEffect(function () {
    const p = document.getElementById("typewriter-text");

    const phrases = [
      "El torneo más importante de la comunidad.",
      "18 y 19 de Diciembre por Facebook Gaming.",
      "16 participantes, un solo vencedor.",
      "patrocinado por plastic hero community.",
    ];

    new TypeWriter(p, {
      strings: phrases,
      autoStart: true,
      loop: true,
      pauseFor: 5000,
      delay: 50,
      deleteSpeed: 15,
    });
  }, []);

  return (
    <p
      id="typewriter-text"
      className={classNames("text-white text-center", styles["writer-text"])}
    ></p>
  );
}
