import ScoreForm from "components/ScoreForm";
import LoadingContext from "context/loading";
import RegisteredContext from "context/registered";
import { useContext, useState } from "react";
import { useForm } from "react-hook-form";
import { saveScores } from "services/player";
import swal from "sweetalert2";

export default function Modal() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const { setIsLoading } = useContext(LoadingContext);
  const { setIsRegistered } = useContext(RegisteredContext);

  const [firstScore, setFirstScore] = useState({});
  const [secondScore, setSecondScore] = useState({});
  const [thirdScore, setThirdScore] = useState({});

  async function onSubmit(formData) {
    if (!firstScore.score || !secondScore.score || !thirdScore.score) {
      return;
    }

    try {
      setIsLoading(true);

      const data = await saveScores({
        ...formData,
        firstScore,
        secondScore,
        thirdScore,
      });

      setIsLoading(false);

      localStorage.setItem("is-registered", "true");

      const modalInstance = bootstrap.Modal.getInstance(
        document.getElementById("staticBackdrop")
      );

      modalInstance.hide();

      setIsRegistered(true);

      await swal.fire({
        title: "Registro completado",
        text: "¡Gracias por participar! los 32 participantes serán anunciados el 11 de Diciembre por la página de Arvelplay0907",
        icon: "success",
        confirmButtonText: "De acuerdo",
      });
    } catch (e) {
      setIsLoading(false);

      if (Array.isArray(e.errors)) {
        const { msg } = e.errors[0];

        await swal.fire({
          title: "Error al registrar jugador",
          text: msg,
          icon: "error",
          confirmButtonText: "De acuerdo",
        });
      }
    }
  }

  return (
    <div
      className="modal fade"
      id="staticBackdrop"
      data-bs-backdrop="static"
      data-bs-keyboard="false"
      tabIndex="-1"
      aria-labelledby="staticBackdropLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content">
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="modal-header">
              <h5 className="modal-title" id="staticBackdropLabel">
                Enviar puntuaciones
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <ScoreForm
                register={register}
                errors={errors}
                setFirstScore={setFirstScore}
                setSecondScore={setSecondScore}
                setThirdScore={setThirdScore}
              />
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-bs-dismiss="modal"
              >
                Cancelar
              </button>
              <button type="submit" className="btn btn-primary">
                Enviar
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
