function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export default function LeaderboardResponsive({ leaderboard }) {
  return (
    <table className="table table-light table-bordered align-middle">
      <thead className="table-dark">
        <tr>
          <th scope="col">Posición</th>
          <th scope="col">Jugador</th>
          <th scope="col">Total</th>
        </tr>
      </thead>
      <tbody>
        {leaderboard.map(({ nickname, total }, index) => (
          <tr key={nickname}>
            <th scope="row" className="ps-4">
              {index + 1}
            </th>
            <td>{nickname}</td>
            <td>{numberWithCommas(total)}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}
