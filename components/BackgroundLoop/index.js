import styles from "./styles.module.css";

export default function BackgroundLoop() {
  return (
    <div className={styles["background-container"]}>
      <div className={styles["background-overlay"]}></div>
      <video
        autoPlay
        loop
        muted
        className={styles["background-video"]}
        type="video/mp4"
      >
        <source src="/fire-loop.mp4" type="video/mp4" />
      </video>
    </div>
  );
}
