import Screenshot from "components/Screenshot";

export default function ScoreForm({
  register,
  errors,
  setFirstScore,
  setSecondScore,
  setThirdScore,
}) {
  return (
    <>
      <div className="mb-3">
        <label htmlFor="nickname" className="form-label">
          Nombre de jugador (Nickname) <span className="text-danger">*</span>
        </label>
        <input
          id="nickname"
          type="text"
          className={
            errors.nickname ? "form-control is-invalid" : "form-control"
          }
          {...register("nickname", { required: true, maxLength: 100 })}
        />
        {errors.nickname ? (
          <div className="invalid-feedback">
            El nombre de jugador es inválido
          </div>
        ) : null}
      </div>

      <div className="mb-3">
        <label htmlFor="discord" className="form-label">
          Usuario de Discord <span className="text-danger">*</span>
        </label>
        <input
          id="discord"
          type="text"
          className={
            errors.discord ? "form-control is-invalid" : "form-control"
          }
          {...register("discord", { required: true, maxLength: 100 })}
        />
        {errors.discord ? (
          <div className="invalid-feedback">
            El usuario de discord es inválido
          </div>
        ) : (
          <div className="form-text">Por ejemplo Usuario #0000</div>
        )}
      </div>

      <Screenshot song="1.618" artist="Allegaeon" onChange={setFirstScore} />
      <Screenshot
        song="Le serment de tsion"
        artist="First Fragment"
        onChange={setSecondScore}
      />
      <Screenshot
        song="Paganini No. 24"
        artist="InfamousColin"
        onChange={setThirdScore}
      />
    </>
  );
}
