import LoadingContext from "context/loading";
import PropTypes from "prop-types";
import { useContext, useRef, useState } from "react";
import { scanScreenshot } from "services/player";
import swal from "sweetalert2";

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export default function Screenshot({ song, artist, onChange }) {
  const [score, setScore] = useState(null);
  const [isFC, setIsFC] = useState(false);
  const { setIsLoading } = useContext(LoadingContext);
  const inputRef = useRef(null);

  async function onFileChange(event) {
    if (event.target.files.length) {
      setIsLoading(true);

      const result = await scanScreenshot(event.target.files[0], song, artist);

      setIsLoading(false);

      if (!result.isValid) {
        await swal.fire({
          title: "Error al procesar puntuación",
          text: `La captura seleccionada no pertenece a la canción "${song} - ${artist}"`,
          icon: "error",
          confirmButtonText: "De acuerdo",
        });

        inputRef.current.value = "";
      } else if (!result.score) {
        await swal.fire({
          title: "Error al procesar puntuación",
          text: "La captura es ilegible, favor de subir una captura directamente del juego",
          icon: "error",
          confirmButtonText: "De acuerdo",
        });

        inputRef.current.value = "";
      } else {
        const { score, isFC, filename } = result;

        setScore(score);
        setIsFC(isFC);
        onChange({ score, isFC, filename });
      }
    }
  }

  return (
    <div className="mb-3">
      <label htmlFor="formFile" className="form-label">
        Captura de{" "}
        <i>
          {song} - {artist}
        </i>
        {score > 0 ? <i className="ms-2 fas fa-check text-success"></i> : null}
      </label>
      <input
        ref={inputRef}
        className="form-control"
        type="file"
        onChange={onFileChange}
      />
      {score ? (
        <label
          htmlFor="formFile"
          className="form-label mt-2 text-success"
          style={{ fontSize: "20px" }}
        >
          Puntuación:{" "}
          <b>
            {numberWithCommas(score)}
            {isFC ? " 👑 (Full Combo)" : null}
          </b>
        </label>
      ) : null}
    </div>
  );
}

Screenshot.propTypes = {
  song: PropTypes.string.isRequired,
  artist: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};
