const FIRST_SONG = "1.618";
const SECOND_SONG = "Le Serment De Tsion";
const THIRD_SONG = "Paganini No. 24";

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export default function Leaderboard({ leaderboard }) {
  return (
    <table className="table table-light table-bordered align-middle">
      <thead className="table-dark">
        <tr>
          <th scope="col">Posición</th>
          <th scope="col">Jugador</th>
          <th scope="col">Discord</th>
          <th scope="col">{FIRST_SONG}</th>
          <th scope="col">{SECOND_SONG}</th>
          <th scope="col">{THIRD_SONG}</th>
          <th scope="col">Capturas</th>
          <th scope="col">Total</th>
        </tr>
      </thead>
      <tbody>
        {leaderboard.map(({ nickname, discord, scores, total }, index) => {
          const firstSong = scores.find((s) => s.song === FIRST_SONG);
          const secondSong = scores.find((s) => s.song === SECOND_SONG);
          const thirdSong = scores.find((s) => s.song === THIRD_SONG);

          return (
            <tr key={nickname}>
              <th scope="row" className="ps-4">
                {index + 1}
              </th>
              <td>{nickname}</td>
              <td>{discord}</td>
              <td className={firstSong.isFC ? "fullcombo" : ""}>
                {numberWithCommas(firstSong.score)}
              </td>
              <td className={secondSong.isFC ? "fullcombo" : ""}>
                {numberWithCommas(secondSong.score)}
              </td>
              <td className={thirdSong.isFC ? "fullcombo" : ""}>
                {numberWithCommas(thirdSong.score)}
              </td>
              <td>
                <div className="d-flex flex-column">
                  <a
                    className="btn-link"
                    href={`${process.env.NEXT_PUBLIC_S3_URL}/${firstSong.filename}`}
                  >
                    <i className="fas fa-download"></i> Captura de {FIRST_SONG}
                  </a>
                  <a
                    className="btn-link"
                    href={`${process.env.NEXT_PUBLIC_S3_URL}/${secondSong.filename}`}
                  >
                    <i className="fas fa-download"></i> Captura de {SECOND_SONG}
                  </a>
                  <a
                    className="btn-link"
                    href={`${process.env.NEXT_PUBLIC_S3_URL}/${thirdSong.filename}`}
                  >
                    <i className="fas fa-download"></i> Captura de {THIRD_SONG}
                  </a>
                </div>
              </td>
              <td>{numberWithCommas(total)}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
