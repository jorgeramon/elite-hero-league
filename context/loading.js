import React from "react";

const LoadingContext = React.createContext({
  isLoading: false,
  setIsLoading: function () {},
});

export default LoadingContext;
