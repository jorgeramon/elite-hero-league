import React from "react";

const RegisteredContext = React.createContext({
  isRegistered: false,
  setIsRegistered: function () {},
});

export default RegisteredContext;
