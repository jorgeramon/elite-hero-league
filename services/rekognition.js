import {
  DetectTextCommand,
  RekognitionClient,
} from "@aws-sdk/client-rekognition";

const client = new RekognitionClient({
  region: process.env.NEXT_PUBLIC_S3_REGION,
  credentials: {
    accessKeyId: process.env.NEXT_PUBLIC_S3_ACCESS_KEY,
    secretAccessKey: process.env.NEXT_PUBLIC_S3_SECRET_ACCESS,
  },
});

export function analyzeText(filename) {
  return client.send(
    new DetectTextCommand({
      Image: {
        S3Object: { Bucket: process.env.NEXT_PUBLIC_S3_BUCKET, Name: filename },
      },
    })
  );
}
