import { analyzeText } from "services/rekognition";
import { uploadFile } from "services/s3";
import { v4 } from "uuid";

export function getLeaderboard() {
  return fetch(`${process.env.NEXT_PUBLIC_API}/players`).then(
    async (response) => {
      const data = await response.json();
      return response.ok ? Promise.resolve(data) : Promise.reject(data);
    }
  );
}

export function getClassified() {
  return fetch(`${process.env.NEXT_PUBLIC_API}/players/classified`).then(
    async (response) => {
      const data = await response.json();
      return response.ok ? Promise.resolve(data) : Promise.reject(data);
    }
  );
}

export function saveScores({
  firstScore,
  secondScore,
  thirdScore,
  nickname,
  discord,
}) {
  return fetch(`${process.env.NEXT_PUBLIC_API}/players`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      nickname,
      discord,
      scores: [
        {
          song: "1.618",
          score: firstScore.score,
          isFC: firstScore.isFC,
          filename: firstScore.filename,
        },
        {
          song: "Le Serment De Tsion",
          score: secondScore.score,
          isFC: secondScore.isFC,
          filename: secondScore.filename,
        },
        {
          song: "Paganini No. 24",
          score: thirdScore.score,
          isFC: thirdScore.isFC,
          filename: thirdScore.filename,
        },
      ],
    }),
  }).then(async (response) => {
    const data = await response.json();
    return response.ok ? Promise.resolve(data) : Promise.reject(data);
  });
}

export async function scanScreenshot(file, song, artist) {
  let filename;

  switch (file.type) {
    case "image/png":
      filename = `${v4()}.png`;
      break;

    case "image/jpg":
    case "image/jpeg":
      filename = `${v4()}.jpg`;
      break;

    case "image/gif":
      filename = `${v4()}.gif`;
      break;

    default:
      filename = v4();
  }

  await uploadFile(file, filename);

  const { TextDetections } = await analyzeText(filename);

  return {
    score: getScoreFromScreenshot(TextDetections),
    filename,
    isFC: screenshotContains(TextDetections, "full combo"),
    isValid:
      screenshotContains(TextDetections, song) &&
      screenshotContains(TextDetections, artist) &&
      screenshotContains(TextDetections, "clone hero"),
  };
}

function getScoreFromScreenshot(detections) {
  return detections
    .map(({ DetectedText }) =>
      parseInt(DetectedText.replace(",", "").replace(".", ""))
    )
    .filter((scores) => !!scores)
    .reduce(
      (accumulator, value) => (value > accumulator ? value : accumulator),
      0
    );
}

function screenshotContains(detections, text) {
  return detections.some(({ DetectedText }) =>
    DetectedText.toLowerCase().includes(text.toLowerCase())
  );
}
