import { PutObjectCommand, S3Client } from "@aws-sdk/client-s3";

const client = new S3Client({
  region: process.env.NEXT_PUBLIC_S3_REGION,
  credentials: {
    accessKeyId: process.env.NEXT_PUBLIC_S3_ACCESS_KEY,
    secretAccessKey: process.env.NEXT_PUBLIC_S3_SECRET_ACCESS,
  },
});

export function uploadFile(file, filename) {
  return client.send(
    new PutObjectCommand({
      Body: file,
      Bucket: process.env.NEXT_PUBLIC_S3_BUCKET,
      Key: filename,
    })
  );
}
