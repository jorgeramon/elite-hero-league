import "animate.css";
import Loading from "components/Loading";
import LoadingContext from "context/loading";
import RegisteredContext from "context/registered";
import Head from "next/head";
import Script from "next/script";
import { useState } from "react";
import "styles/globals.css";

export default function Aplication({ Component, pageProps }) {
  const [isLoading, setIsLoading] = useState(false);
  /*const [isRegistered, setIsRegistered] = useState(
    typeof window !== "undefined"
      ? !!localStorage.getItem("is-registered")
      : false
  );*/
  const [isRegistered, setIsRegistered] = useState(false);

  const seo = [
    {
      content: "Elite Hero League PowerLite",
      type: ["title", "og:title", "twitter:title"],
    },
    {
      content:
        "El torneo más importante de la comunidad. 18 y 19 de Diciembre por Facebook Gaming.",
      type: ["description", "og:description", "twitter:description"],
    },
    {
      content: "https://ehl.plastichero.rocks/ehl-logo.png",
      type: ["image", "og:image", "twitter:image"],
    },
  ];
  return (
    <>
      <Head>
        <title>Elite Hero League PowerLite</title>
        <link
          href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
          crossOrigin="anonymous"
        />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin="true"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Anton&display=swap"
          rel="stylesheet"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Roboto+Condensed&display=swap"
          rel="stylesheet"
        />
        <link
          rel="stylesheet"
          href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"
          integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm"
          crossOrigin="anonymous"
        />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        {seo
          .map(({ type, content }) =>
            type.map((t) => <meta key={t} name={t} content={content} />)
          )
          .flat()}
      </Head>
      <LoadingContext.Provider value={{ isLoading, setIsLoading }}>
        <RegisteredContext.Provider value={{ isRegistered, setIsRegistered }}>
          <Loading show={isLoading} />
          <Component {...pageProps} />
        </RegisteredContext.Provider>
      </LoadingContext.Provider>
      <Script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossOrigin="anonymous"
      ></Script>
      <Script
        src="https://kit.fontawesome.com/d5ba453ebe.js"
        crossOrigin="anonymous"
      ></Script>
    </>
  );
}
