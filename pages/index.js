import classNames from "classnames";
import BackgroundLoop from "components/BackgroundLoop";
import ImportantButtons from "components/ImportantButtons";
import LeaderboardResponsive from "components/LeaderboardResponsive";
import TypeWriterText from "components/TypeWriterText";
import YoutubeVideo from "components/YoutubeVideo";
import LoadingContext from "context/loading";
import { useContext, useEffect, useState } from "react";
import { getClassified } from "services/player";
import styles from "styles/home.module.css";

export default function Home() {
  const [clicks, setClicks] = useState(0);
  const [scores, setScores] = useState([]);
  const { setIsLoading } = useContext(LoadingContext);

  useEffect(function () {
    setIsLoading(true);

    async function fetchLeaderboard() {
      try {
        const { data } = await getClassified();
        setScores(data);
        setIsLoading(false);
      } catch (e) {
        setIsLoading(false);
      }
    }

    fetchLeaderboard();
  }, []);

  return (
    <>
      <BackgroundLoop />

      <div className="d-flex flex-column vw-100 vh-100 align-items-center">
        <img
          src={clicks >= 3 ? "/arvel_easteregg.png" : "/ehl-logo.png"}
          className={classNames(
            styles.logo,
            "mt-4",
            clicks >= 3 ? "animate__animated animate__backInDown" : ""
          )}
          onClick={() => setClicks(clicks + 1)}
        />

        <div className="mt-4 w-100" id="typewriter-container">
          <TypeWriterText />
        </div>

        <div id="youtube-container" className="mt-3">
          <YoutubeVideo embedId="O8JEUrGD978" />
        </div>

        <div className="mt-5 w-100 d-flex flex-column justify-content-center align-items-center classified">
          <h1 className="text-white">Clasificados 🏆</h1>
          <div className="col-md-6 col-10">
            <LeaderboardResponsive leaderboard={scores} />
          </div>
        </div>

        <div className="mt-3 w-100">
          <ImportantButtons />
        </div>

        {/*<Modal />*/}
      </div>
    </>
  );
}
