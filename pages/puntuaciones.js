import BackgroundLoop from "components/BackgroundLoop";
import Leaderboard from "components/Leaderboard";
import LeaderboardResponsive from "components/LeaderboardResponsive";
import LoadingContext from "context/loading";
import { useContext, useEffect, useState } from "react";
import { getLeaderboard } from "services/player";
import swal from "sweetalert2";

export default function Scores() {
  const [scores, setScores] = useState([]);
  const { setIsLoading } = useContext(LoadingContext);

  useEffect(function () {
    setIsLoading(true);

    async function fetchLeaderboard() {
      try {
        const { data } = await getLeaderboard();
        setScores(data);
        setIsLoading(false);
      } catch (e) {
        setIsLoading(false);
        await swal.fire({
          title: "Error al obtener puntuaciones",
          text: "Ocurrió un problema en el servidor, intenta de nuevo",
          icon: "error",
          confirmButtonText: "De acuerdo",
        });
      }
    }

    fetchLeaderboard();
  }, []);

  return (
    <>
      <BackgroundLoop />

      <div className="leaderboard mt-4 container">
        <div className="d-flex flex-column justify-content-center align-items-center">
          <h1 className="text-white text-uppercase">Tabla de puntuaciones</h1>
          <a
            className="btn btn-success"
            href={`${process.env.NEXT_PUBLIC_API}/players/leaderboard`}
          >
            <i className="fas fa-file-download"></i> Descargar en Excel
          </a>
        </div>

        <div className="d-none d-md-block mt-5">
          <Leaderboard leaderboard={scores} />
        </div>

        <div className="d-block d-md-none mt-5">
          <LeaderboardResponsive leaderboard={scores} />
        </div>
      </div>
    </>
  );
}
